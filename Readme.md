# About
The goal of this project is to create an anarchist commune in unclaimed stretches of Antarctica.
We acknowledge the implausability of this task, but wish to remain optimistic about the possibility of it happening anyway.
We created this git page with the intention of allowing people to contribute their knowledge, technical expertise, ideas, and etc, so as to further the project.

# License
Any work contributed will be considered as donated to the Apollonia Cooperative.
Any redistribution of this work is permitted, provided that it links back to the original project in an easy to identify fashion, accurately reflects our views and intentions, and if any donations are recieved, they are to be given to the original project, preferably through the official donation links.

# Discuss
Visit our matrix discussion board at: https://matrix.to/#/#apollonia:matrix.org

# Contribute
Feel free to put forward ideas and suggestions for the community to consider, by submitting a merge request.
We build and maintain the website using [hugo](https://gohugo.io), which using markdown and it's own templating system.

Alternatively, you can contribute to one of the following cryptocurrency addresses:

btc: bc1qr8q46yfcaf57vy6445t20cqma8qkyjx49qm3gt

eth: 0xb9F5160E3347736248ef32f724a923f17Ca2841c

monero: 4A4LJqFGyRqeMn1BHfdhyEV5EFwCEB3a63MVpFvgE91rEZjEVwzMNiQR98Tkovo8JgV1HtUJW7pxmCSUkn8UWd3DTEBVav8
