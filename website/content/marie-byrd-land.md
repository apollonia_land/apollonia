---
title: "Marie Byrd Land"
date: 2021-11-16T13:58:29-05:00
draft: false
---
![Marie Byrd Land](/images/marie_byrd_land.png)
Marie Byrd Land is a wedge of land in Antarctica, which is currently unclaimed by any country, and due to the nature of the Antarctic Treaty System, it cannot be claimed by a country.
It should be noted that no one can be sure what will happen when the mining ban comes up for review.
Due to the harsh and unforgiving nature of the land, living here will not be for the faint of heart, but those who are willing to live in those conditions are welcome with us.
See the [Wikipedia article](https://en.wikipedia.org/wiki/Marie_Byrd_Land) for more information, and the source of the above image.
The exact extent of the land varies with the source used. Certain sources cite an old definition by the United States, which puts the eastern most edge at approximately 103 degrees west. However, the name is often also applied to all the unclaimed land in Antarctica, which further extends the eastern edge to 90 degrees west.
