---
title: "Is Apollonia Plausible?"
date: 2021-11-18T05:52:40-05:00
draft: false
---

In short, the answer is yes, it does seem plausible, but that is not to imply it will be easy.

## Can we legally create our own base in Antarctica?
Going off of historic precedent, yes.
In 1987, Greenpeace, a non-governmental organization, built [World Park Base](https://en.wikipedia.org/wiki/World_Park_Base) with the intention of preserving the unique Antarctic environment.
[Here](https://www.greenpeace.org/usa/victories/creating-the-world-park-antarctica/) is a Greenpeace article explaining the highlights of their mission, and [here](https://web.archive.org/web/20050121020047/http://archive.greenpeace.org/gopher/campaigns/landecol/1994/antarc4.txt) is an article going into significant detail about how they did it.

## What about shelter?
Again looking at Greenpeace's precedent, a non-governmental organization can successfully build suitable shelter for the harsh Antarctic environment.
[This](https://media.greenpeace.org/archive/Greenpeace-World-Park-Base--Antarctica-27MZIFLGPWAH.html) picture of World Park Base, before it was taken down, can give us an idea of what our habitat may look like.
Further steps, such as packing snow around the outside base, can be taken to increase the base's insulation and make heating easier.
We can further look to people living in the Artic for inspiration on how to build shelter.
Indigenous peoples are an especially valuable resource, because if an emergency arose, their centuries and millenia of experience living in the north can provide a template for improvised shelter.

## How will we eat?
Food is going to be one of our more challenging issues.
For multiple reasons, we will find it difficult, even impossible, to grow our own food.
First, Antarctica is the last bastion of untamed land in the world, and we should aspire to maintain it's unique beauty.
It has become abuntantly clear that merely living on the land is rife with long term consequences, so we must strive to live *with* the land.
Secondly, even if we chose to exploit the land, our hands are legally tied by international law.
Per the Antarctic treaty, no foreign species of plants or animals may be imported to the continent, and if we wish to keep our presence on the continent unchallenged, we must comply.
Finally, even if neither of the above were an issue, the foods we eat simply cannot survive the harsh environment.
They evolved under very different conditions, and the adaptations they developed are not compatible with Antarctica.

We are left with local species and importing our food.
Fish will likely be the bulk of what we eat, as they are the only abundant source of food, and because the birds, mamals, and plants native to the continent are [protected](https://www.congress.gov/congressional-report/104th-congress/house-report/593).
For plants, we may be able to cultivate them, as the treaty stipulates that they cannot be harmed or removed in such a quantity as to affect their distribution/abundance.
We could potentially sustainably harvest them, or perhaps harvest an initial quantity and cultivate it from there, however, we should ask for a lawyer's opinion to be certain.

Importing food is the predominent strategy.
Unfortunately, this will imply some level of reliance on the outside world, and require some form of income.
Income could be earned via donations, or more likely by remote labor.
