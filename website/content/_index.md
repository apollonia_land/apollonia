---
title: "Apollonia"
date: 2021-11-10T13:41:09-05:00
draft: false
---

# Apollonia
## About us
We are an anarchist anti-nation which intends to live in and with Marie Byrd land in Antartica. 
We seek to create a home for all LGBTQIA2+ people, so we may be free from the repressive and/or dangerous places we live.
LGBTQIA2+ allies are welcome provided that they contribute to a community of love and mutual respect.

## Our goals
We seek to raise funds which will go towards creating a safe, warm place to live year-round in Marie Byrd land.
If we are able to accomplish this, we further intend to establish a mutual aid fund which will be used to transport queer and trans people in need of a home to our community. 

## Contact
[Chat on Matrix](https://matrix.to/#/#apollonia:matrix.org)

[Email](mailto:apollonialand@tutanota.com)

## Donate
btc: bc1qr8q46yfcaf57vy6445t20cqma8qkyjx49qm3gt

eth: 0xb9F5160E3347736248ef32f724a923f17Ca2841c

monero: 4A4LJqFGyRqeMn1BHfdhyEV5EFwCEB3a63MVpFvgE91rEZjEVwzMNiQR98Tkovo8JgV1HtUJW7pxmCSUkn8UWd3DTEBVav8
