* Territory
[[file:marie-byrd-land.png][Claimed Area Map]]
[[file:temps.png][Temperature Map]]
[[file:precip.gif][Precipitation Map]]

* Legality
- United States nationals are required to notify the Department of State at least 3 months prior to visiting.
[[https://travel.state.gov/content/travel/en/international-travel/International-Travel-Country-Information-Pages/Antarctica.html][United States travel information]]
	The language of the page only says provide notification, not recieve permission.
- [[https://en.wikipedia.org/wiki/World_Park_Base][World Park Base]]
	[[https://web.archive.org/web/20050121020047/http://archive.greenpeace.org/gopher/campaigns/landecol/1994/antarc4.txt]]
	[[https://web.archive.org/web/20041222131452/http://archive.greenpeace.org/comms/climate/polartour/pt01.html]]
	[[https://www.greenpeace.org/usa/victories/creating-the-world-park-antarctica/]]

* Settlement sites
- Byrd Station 80 00'53''S x 119 33' 56''W
  Seasonal research station, closed in 2005, take over?
- Near the coast for greater warmth

* Survival
** Shelter
	- Ice is a good insulator, could build igloos/dig into the ice
	- Build underground?
		+ Permafrost probably too tough to dig through
	- Thoughts
		+ Build on pillars, to raise structure abover permafrost
		+ Pack building with snow for insulation
		+ Insulate walls to further keep heat in
		+ Entrance elevated and without snow pack to minimize accumulation near it, and for easy access
		+ Build for accessability
** Power
		- Solar in the summer
		- Wind for the winter
			Probably have to ration power
		- Giant battery? A la Musk
** Food
		- Can anything be grown in the summer?
			+ Cannot import flora or fauna into the continent
			+ we'd have to survive on imports and native species
		- Fish
		- Penguins? (Probably not)
		- Most like have to import more as well
			+ How to pay for it?
				Software development?
			+ Where to import from?
				Nearby ports?


* Who should go first
	- Doctor, probably two
	- People who can catch and prepare fish
	- Engineer
	- Sailor

* Life in Artic for Comparison
  - [[https://backpackingman.com/what-it-is-like-living-in-the-arctic/][What is it like living in the artic]]


